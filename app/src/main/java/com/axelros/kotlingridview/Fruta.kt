package com.axelros.kotlingridview

class Fruta(name:String, image:Int) {
    var name:String = ""
    var image: Int = 0

    init {
        this.name = name
        this.image = image
    }
}