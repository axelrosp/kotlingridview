package com.axelros.kotlingridview

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*

class AdapterCustom(var context: Context, items:ArrayList<Fruta>):BaseAdapter() {

    var items:ArrayList<Fruta>? = null

    init {
        this.items = items
    }

    override fun getView(position: Int, convertView: View?, p2: ViewGroup?): View {
        var view = convertView
        var holder:ViewHolder? = null

        if(view == null){
            view = LayoutInflater.from(context).inflate(R.layout.grid_item_template, null)
            holder = ViewHolder(view)
            view.tag = holder

        }else{
            holder = view.tag as? ViewHolder
        }

        val item = items?.get(position)
        holder?.name?.text = item?.name
        holder?.image?.setImageResource(item?.image!!)


        holder?.btnName?.setOnClickListener {
            Toast.makeText(context, holder.name?.text, Toast.LENGTH_LONG).show()
        }

        return view!!

    }

    override fun getItem(position: Int): Any {
        return items?.get(position)!!
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return items?.count()!!
    }

    private class ViewHolder(view:View){
        var name:TextView? = null
        var image:ImageView? = null
        var celda:LinearLayout? = null
        var btnName: Button? = null

        init {
            this.name = view.findViewById(R.id.tvName)
            this.image = view.findViewById(R.id.ivImage)
            this.celda = view.findViewById(R.id.celda)
            this.btnName = view.findViewById(R.id.btnName)
        }
    }
}